#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define PAGE_SIZE (size_t) sysconf(_SC_PAGESIZE)
#define OUTPUT_HEX_SIZE 16

struct file_comparison {
  const char *fptr; // ptr to start of file
  size_t fsize;     // file size
  int fd;           // file descriptor
};

static void prv_print_help(void) {
  printf("File Comparison:\n\
   Given the paths to two files, compares them\n\
   usage: file_comparison [options] file_path1 file_path2\n\
    options:\n\
    -h, --help  displays this menu\n\
  ");
}

static void prv_lock_files(struct file_comparison *f1,
                           struct file_comparison *f2) {
  struct flock file_lock1 = {
      .l_type = F_RDLCK,
      .l_whence = SEEK_SET,
      .l_start = 0,
      .l_len = f1->fsize,
      .l_pid = 0,
  };

  struct flock file_lock2 = {
      .l_type = F_RDLCK,
      .l_whence = SEEK_SET,
      .l_start = 0,
      .l_len = f2->fsize,
      .l_pid = 0,
  };

  if (fcntl(f1->fd, F_SETLK, &file_lock1) < 0) {
    perror("File 1 busy");
    exit(1);
  }
  if (fcntl(f2->fd, F_SETLK, &file_lock2) < 0) {
    perror("File 1 busy");
    exit(1);
  }
}

static void prv_unlock_files(struct file_comparison *f1,
                             struct file_comparison *f2) {
  struct flock file_lock1 = {
      .l_type = F_UNLCK,
      .l_whence = SEEK_SET,
      .l_start = 0,
      .l_len = f1->fsize,
      .l_pid = 0,
  };

  struct flock file_lock2 = {
      .l_type = F_UNLCK,
      .l_whence = SEEK_SET,
      .l_start = 0,
      .l_len = f2->fsize,
      .l_pid = 0,
  };

  if (fcntl(f1->fd, F_SETLK, &file_lock1) < 0) {
    perror("File 1 busy");
    exit(1);
  }
  if (fcntl(f2->fd, F_SETLK, &file_lock2) < 0) {
    perror("File 1 busy");
    exit(1);
  }
}

// Algorithm to locate first difference in a block of memory
// It works by divide and conquer, and will run in O(log(n)) with max PAGE_SIZE
// elements
static size_t prv_handle_mismatch(const char *f1, const char *f2, size_t size) {
  const char *ptr1 = f1;
  const char *ptr2 = f2;
  // Area to check - area will be <= page size
  uint16_t block_size = (uint16_t)size;
  uint16_t read_size = UINT16_MAX;
  while (block_size > 1) {
    if (read_size != 1) {
      read_size = block_size / 2;
    }
    // Check if mismatch in current block
    if (memcmp(ptr1, ptr2, read_size)) {
      block_size = read_size;
    } else {
      ptr1 += read_size;
      ptr2 += read_size;
      block_size = block_size - read_size;
    }
  }
  return (size_t)(ptr1 - f1);
}

static size_t prv_compare(struct file_comparison *f1,
                          struct file_comparison *f2) {
  size_t max_size = f1->fsize < f2->fsize ? f1->fsize : f2->fsize;
  size_t max_page = max_size / PAGE_SIZE;
  size_t index = 0;
  size_t offset = 0;

  // Compare each sequential full page
  while (index < max_page) {
    if (memcmp(f1->fptr + index * PAGE_SIZE, f2->fptr + index * PAGE_SIZE,
               PAGE_SIZE) != 0) {
      offset += prv_handle_mismatch(f1->fptr + index * PAGE_SIZE,
                                    f2->fptr + index * PAGE_SIZE, PAGE_SIZE);
      return offset;
    }
    ++index;
    offset += PAGE_SIZE;
  }
  // Handle end data that did not takeup full page
  if (memcmp(f1->fptr + index * PAGE_SIZE, f2->fptr + index * PAGE_SIZE,
             max_size - max_page * PAGE_SIZE) != 0) {
    offset += prv_handle_mismatch(f1->fptr + index * PAGE_SIZE,
                                  f2->fptr + index * PAGE_SIZE,
                                  max_size - max_page * PAGE_SIZE);
  }
  // If last character identical, files identical/different file size
  if (f1->fptr[offset] == f2->fptr[offset]) {
    if (f1->fsize == f2->fsize) {
      return 0; // files are identical
    } else {
      return max_size;
    }
  }
  return offset;
}

static void prv_print_bytes(struct file_comparison *f, size_t offset) {
  for (uint8_t i = 0; i < 16; i++) {
    if (offset + i > f->fsize) {
      printf(" XX");
    } else {
      printf(" %02X", (unsigned int)(f->fptr[offset + i] & 0xFF));
    }
  }
  printf("\n");
}

int main(int argc, char *argv[]) {
  printf("Launching Binary Comparer...\n");

  // Check argv[1] exists
  if (argc < 2) {
    printf("ERROR: Invalid Input\n");
    prv_print_help();
    return EINVAL;
  }

  // Handle help
  if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
    prv_print_help();
    return 0;
  }

  // Check both required arguments present
  if (argc != 3) {
    printf("ERROR: Invalid Input\n");
    prv_print_help();
    return EINVAL;
  }
  printf("Files to compare: %s, %s\n", argv[1], argv[2]);

  // Create clocks for benchmark
  clock_t start, end;
  struct file_comparison file1, file2;
  start = clock();

  // Open files
  file1.fd = open(argv[1], O_RDONLY);
  file2.fd = open(argv[2], O_RDONLY);
  if (file1.fd < 0 || file2.fd < 0) {
    perror("Could not open input file");
  }

  // Get file info from descriptor
  struct stat st1, st2;
  if (fstat(file1.fd, &st1) == -1 || fstat(file2.fd, &st2) == -1) {
    perror("Error getting file information");
  }
  file1.fsize = st1.st_size;
  file2.fsize = st2.st_size;

  // Use Unix advisory locking to ensure file stays in steady state
  prv_lock_files(&file1, &file2);

  // Store void ptr for munmap call
  void *mem_file1 =
      mmap(NULL, file1.fsize, PROT_READ, MAP_PRIVATE, file1.fd, 0);
  void *mem_file2 =
      mmap(NULL, st2.st_size, PROT_READ, MAP_PRIVATE, file2.fd, 0);
  if (mem_file1 == MAP_FAILED || mem_file2 == MAP_FAILED) {
    perror("Could not mmap");
    return errno;
  }
  file1.fptr = mem_file1;
  file2.fptr = mem_file2;

  // Handle base case where files differ at first byte
  if (file1.fptr[0] != file2.fptr[0]) {
    printf("OFFSET: %d\n", 0);
    printf("Printing 16 bytes after offset in hexadecimal\n");
    prv_print_bytes(&file1, 0);
    prv_print_bytes(&file2, 0);
  } else {
    // Get offset of first difference
    size_t offset = prv_compare(&file1, &file2);
    if (offset == 0) {
      printf("Files are Identical!\n");
    } else {
      printf("OFFSET: %zu\n", offset);
      printf("Printing 16 bytes after offset in hexadecimal\n");
      prv_print_bytes(&file1, offset);
      prv_print_bytes(&file2, offset);
    }
  }

  // Unmap files
  if (munmap(mem_file1, st1.st_size) < 0 ||
      munmap(mem_file2, st2.st_size) < 0) {
    perror("Unmapping memory failed");
  }

  // Remove advisory lock
  prv_unlock_files(&file1, &file2);

  // Get benchmark
  end = clock();
  double time_taken = ((double)end - (double)start) / (double)CLOCKS_PER_SEC;
  printf("time taken: %2lf milliseconds\n", time_taken * 1000);

  return 0;
}
