# Binary File Comparison for Packet Path

## Description
The program is contained in main.c. It compares two binary files and will print
the offset of the first difference found if there is one, and sixteen bytes of
data for each file starting at the offset. If there is no data (the end of the
file is reached) then XX will be printed in lieu of data. If no difference is
found, the program will output that the files are identical. The program
additionally benchmarks and logs the time the program takes to execute.

## Operating systems
The program was written for Linux (Ubuntu 20.04), as this was specified as one
of the operating systems used in the job description. It uses unix system
calls, and so will only work on compatible OS's.

## Functionality
The program uses an mmap call to access the contents of the binary files as an
array. The mmap call maps the files to the virtual address space, creating
a block which is read only and private to the process running the program. 

mmap was chosen as it prevents the need for buffer allocation. Additionally, as
mmap works by mapping the file data to the address space, we do not need the
overhead of many system calls that fread and malloc/memcpy would require. 

Also, as the spec requires us to find only the first difference, this means that 
we do not necessarily need to read the entire file, we only need to read up to the
first difference (and following 16 bytes) which mmap allows us to do with ease.
In the program, we compare page by page until we find a difference.  This
improves efficiency, as it reduces the number of disc I/O operations, only
requiring them for each page that is actually accessed.

In the specification it says that files will be ~1MB in size.  For 32bit
systems the total address space is 4GB, which these files will comfortably fit
inside.

## Thread Safety
This program is thread safe, as long as other processes comply by Unix advisory
locking protocol.  There is a danger of the file being changed as it is being
read, (ie. if it shrinks, mmap calls become undefined) which can cause errors.
To address this, the file is locked via flock before mmap is called and read
operations commence, and unlocked after munmap called. 

## Runtest.sh
runtest.sh compiles and executes the program with several test files, to check
for different functionality.  It outputs the results to "output.log"
